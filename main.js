var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function (socket) {
	
	var name = 'User_' + socket.id,
		room = 'default';
  	socket.join(room);
  	socket.broadcast.emit('newUser', name);
  	socket.emit('userName', name);

	socket.on('message', function(msg) { 
    	io.sockets.in(room).emit('messageToClients', msg, name);

    socket.on('disconnect', function(socket) {
    	io.sockets.in(room).emit('userDisconnected', name);
    });
});



	//ROOM CHANGE
  	socket.on('newRoom', function (newRoom) {
  		newRoom = newRoom.handleObj.selector;
  		io.sockets.in(room).emit('newRoom', newRoom, name);
  		socket.leave(room);
  		socket.join(newRoom);
  		room = newRoom;
	});

});

http.listen(3000);