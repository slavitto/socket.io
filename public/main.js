var socket = io('http://localhost:3000');

socket.on('userName', function(userName) {
	$('p').html('You\'r username: ' + userName);
});

socket.on('newUser', function(userName) {
	$('#p').html(userName + ' connected!');
});

socket.on('newRoom', function(room, name) {
	$('#p').html('User ' + name + ' changed room to ' + room);
});

$(document).on('click', '#button', msg)
	.bind('keydown', function(e) {
		if (e.keyCode == 13) {
			msg();
		};
});

socket.on('messageToClients', function(msg, name){
	$('textarea').val($('textarea').val() + name + ' : '+ msg +'\n');
});

socket.on('userDisconnected', function(name){
	$('#p').html(name + ' diconnected!');
});


function msg() {
	var message = $('input').val(); 
	socket.emit('message', message);
	$('input').val(null);
}

//ROOM
for (var i = 1; i <= 3; i++) {
	$(document).on('click', '#room' + i, function(newRoom) {
	socket.emit('newRoom', newRoom);
	})
}